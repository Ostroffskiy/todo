function Todo(list) {
    this.list = list;
    this.currentCategory = '';
    function createTask(task) {
        return '<li data-id="' + task.id + '" data-state="' + task.status + '" class="todo__task">'+
                   '<p class="todo__text">' + task.name + '</p>'+
                   '<button class="todo__do">do</input>'+
                   '<button class="todo__del">delete</button>'+
                '</li>';
    };

    this.clearDeleted = function () {
        for (var i = 0; i < this.list.length; i++) {
            if (this.list[i].status === 'deleted') {
                this.list.splice(i, 1);
                i--;
            }
        }
        localStorage.setItem('todoData', JSON.stringify(this.list));
    };

    this.select = function (category) {
        var $collection = $([]);
        if (category !== undefined) {
            this.currentCategory = category;
        }
        for (var i = 0; i < this.list.length; i++) {
            if (category && this.list[i].status != category) continue;
            $collection = $collection.add(createTask(this.list[i]));
        }
        return $collection;
    };

    this.refresh = function () {
        localStorage.setItem('todoData', JSON.stringify(this.list));
        return this.select(this.currentCategory);
    };

    this.setState = function (id, state) {
        var id = parseInt(id);
        this.list.forEach(function (task) {
            if (task.id === id) {
                task.status = state;
                return task;
            }
        });
        return null;
    }


    this.addTask = function (name) {
        var list = this.list;
        var task = {
            name: name,
            status: 'incomplete',
            id: list[list.length-1].id+1
        };
        this.list.push(task);
        localStorage.setItem('todoData', JSON.stringify(this.list));
        return this.refresh();
    }
}

$(function () {

    // Тестовый массив
    console.log(JSON.parse(localStorage.getItem('todoData')));
    var test_list = JSON.parse(localStorage.getItem('todoData')) || [{
        name: 'Изучить JS',
        status: 'completed',
        id: 0
    }, {
        name: 'написать TODO',
        status: 'incomplete',
        id: 1
    }, {
        name: '????',
        status: 'incomplete',
        id: 2
    }, {
        name: 'profit!',
        status: 'incomplete',
        id: 3
    }, {
        name: 'Изучить Angular',
        status: 'deleted',
        id: 4
    }];

    var $container = $('.todo__list');

    var todo = new Todo(test_list);
    $container.append(todo.select());

    $(document).on('click', 'button[data-category]', function (e) {
        var $this = $(this);
        var category = $this.data('category');
        var $title = $('.todo__category');

        switch (category) {
            case 'deleted':
                $title.text('deleted');
                break;
            case 'incomplete':
                $title.text('incomplete');
                break;
            case 'completed':
                $title.text('completed');
                break;
            case '':
                $title.text('');
                break;
        }

        $container.html('').append(todo.select(category));

    });

    $(document).on('click', '.todo__do', function (e) {
        e.preventDefault();
        var $task = $(this).closest('li');
        var currentStatus = $task.data('state');

        if (currentStatus == 'completed') {
            todo.setState($task.data('id'), 'incomplete');
        } else if (currentStatus == 'incomplete') {
            todo.setState($task.data('id'), 'completed');
        } else {
            return false;
        }
        $container.html('').append(todo.refresh());
    });

    $(document).on('click', '.todo__del', function (e) {
        e.preventDefault();
        var $task = $(this).closest('li');
        var currentStatus = $task.data('state');
        if (currentStatus == 'deleted') {
            todo.setState($task.data('id'), 'incomplete');
        } else {
            todo.setState($task.data('id'), 'deleted');
        }
        $container.html('').append(todo.refresh());
    });

    $(document).on('click', '.todo-clear', function (e) {
        e.preventDefault();
        todo.clearDeleted();
        $container.html('').append(todo.refresh());
    });

    $(document).on('submit', '.add-task', function (e) {
        e.preventDefault();
        var $this = $(this);
        var name = $this.find('.task-name').val();
        var new_list = todo.addTask(name);
        $this.get(0).reset();
        $container.html('').append(new_list);
    })
});